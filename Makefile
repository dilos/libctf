#
# This file and its contents are supplied under the terms of the
# Common Development and Distribution License ("CDDL"), version 1.0.
# You may only use this file in accordance with the terms of version
# 1.0 of the CDDL.
#
# A full copy of the text of the CDDL should have accompanied this
# source.  A copy of the CDDL is also available via the Internet at
# http://www.illumos.org/license/CDDL.
#

CC := gcc
SRC := .

PROG= libctf.so.1

COMMON_OBJS = \
	ctf_create.o \
	ctf_decl.o \
	ctf_dwarf.o \
	ctf_error.o \
	ctf_hash.o \
	ctf_labels.o \
	ctf_lookup.o \
	ctf_open.o \
	ctf_types.o \
	ctf_util.o

MERGEQ_OBJS = \
	mergeq.o \
	workq.o

LIST_OBJS = \
	list.o

LIB_OBJS = \
	ctf_convert.o \
	ctf_elfwrite.o \
	ctf_diff.o \
	ctf_lib.o \
	ctf_merge.o \
	ctf_subr.o

OBJECTS = $(COMMON_OBJS) $(LIB_OBJS) $(LIST_OBJS) $(MERGEQ_OBJS)
OBJS = $(OBJECTS:%=objs/%)
MAPFILEDIR = $(SRC)/lib/libctf/common

CPPFLAGS += \
		-I$(SRC)/uts/common		\
		-I$(SRC)/lib/libctf/common	\
		-I$(SRC)/common/ctf		\
		-I$(SRC)/lib/mergeq		\
		-DCTF_OLD_VERSIONS

CFLAGS += -O0 -W -Wall -Wextra -Werror -pedantic
CFLAGS += -m64 -std=gnu99 -g -gdwarf-2 -fPIC -DPIC -msave-args
CFLAGS += -ggdb3 
LDFLAGS += -lelf -ldwarf -lavl -lc

all: objs $(PROG)

install: all
	cp -f $(PROG) /lib/amd64/

clean:
	rm -f $(PROG) $(OBJS)
	rm -rf objs

objs/%.o: $(SRC)/common/ctf/%.c
	$(CC) $(CPPFLAGS) $(CFLAGS) -c $< -o $@

objs/%.o: $(SRC)/common/list/%.c
	$(CC) $(CPPFLAGS) $(CFLAGS) -c $< -o $@

objs/%.o: $(SRC)/lib/libctf/common/%.c
	$(CC) $(CPPFLAGS) $(CFLAGS) -c $< -o $@

objs/%.o: $(SRC)/lib/mergeq/%.c
	$(CC) $(CPPFLAGS) $(CFLAGS) -c $< -o $@

$(PROG): $(OBJS)
	$(CC) -shared -Wl,-h,$(PROG) $(CFLAGS) $(OBJS) -o $@ $(LDFLAGS) \
		-Wl,-M,$(MAPFILEDIR)/mapfile-vers

objs:
	mkdir -p $@
